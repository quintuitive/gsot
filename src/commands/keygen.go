package commands

import (
	"crypto/rand"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"os"
	"sotlib"

	"github.com/spf13/cobra"
)

var keygenCmd = &cobra.Command{
	Use:    "keygen",
	Short:  "creates keys",
	Long:   `Creates keys using random generator with crypto quality.`,
	PreRun: preKeygen,
	Run:    runKeygen,
}

func initKeygenCmd() {
	keygenCmd.Flags().IntVarP(&settings.Bits, "bits", "b", sotlib.KEY_LEN*8, "the number of bits in the key to create.")
	keygenCmd.Flags().BoolVarP(&settings.Hex, "hex", "", false, "hex output")

	RootCmd.AddCommand(keygenCmd)
}

func preKeygen(cmd *cobra.Command, args []string) {
	// The repository settings are initialized from cached values. Reset to the defaults.
	settings.RepoCfg.Init()
}

func runKeygen(cmd *cobra.Command, args []string) {

	bb := make([]byte, settings.Bits/8)
	_, err := rand.Read(bb)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	var str string
	if settings.Hex {
		str = hex.EncodeToString(bb)
	} else {
		str = base64.StdEncoding.EncodeToString(bb)
	}
	fmt.Println(str)
}
