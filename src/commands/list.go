package commands

import (
	"fmt"
	"os"
	"sotlib"

	"github.com/spf13/cobra"
)

var listCmd = &cobra.Command{
	Use:     "ls",
	Aliases: []string{"list"},
	Short:   "lists the content of a sot repository path",
	Long:    `Similar to the 'ls' utility. Operates over a sot tree.`,
	PreRun:  preList,
	Run:     runList,
}

func initListCmd() {
	listCmd.Flags().StringVarP(&settings.Cred.Password, "password", "", settings.GetPassword(), "the user password")
	listCmd.Flags().StringVarP(&settings.Cred.KeyFile, "key-file", "", settings.GetKeyFile(), "the user provided key file")
	listCmd.Flags().BoolVarP(&settings.LongFormat, "long", "l", false, "long output format")
	listCmd.Flags().BoolVarP(&settings.Versions, "versions", "", false, "show file versions")
	listCmd.Flags().StringVarP(&settings.Saved.RootPath, "repo", "r", "", "the repository")

	RootCmd.AddCommand(listCmd)
}

func preList(cmd *cobra.Command, args []string) {
}

func runList(cmd *cobra.Command, args []string) {

	err := settings.LoadRepoConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	settings.CreateKey()
	err = settings.CreateKey()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	var pp string
	if len(args) > 0 {
		pp = args[len(args)-1]
	} else {
		pp = "/"
	}

	err = sotlib.List(&settings, pp)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
