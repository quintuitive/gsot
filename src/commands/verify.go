package commands

import (
	"fmt"
	"os"
	"sotlib"

	"github.com/spf13/cobra"
)

var verifyCmd = &cobra.Command{
	Use:    "verify",
	Short:  "verifies the repository",
	Long:   `Verifies the repository. Mostly for debugging purposes.`,
	Run:    runVerify,
	PreRun: preVerify,
}

func initVerifyCmd() {
	verifyCmd.Flags().StringVarP(&settings.Cred.Password, "password", "", settings.GetPassword(), "the user password")
	verifyCmd.Flags().StringVarP(&settings.Cred.KeyFile, "key-file", "", settings.GetKeyFile(), "the user provided key file")
	verifyCmd.Flags().StringVarP(&settings.Saved.RootPath, "repo", "r", "", "the repository")

	RootCmd.AddCommand(verifyCmd)
}

func preVerify(cmd *cobra.Command, args []string) {
}

func runVerify(cmd *cobra.Command, args []string) {
	err := settings.LoadRepoConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	settings.CreateKey()
	err = settings.CreateKey()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	err = sotlib.Verify(&settings)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
