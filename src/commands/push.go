package commands

import (
	"fmt"
	"os"
	"sotlib"

	"github.com/spf13/cobra"
)

var pushCmd = &cobra.Command{
	Use:    "push",
	Short:  "push files and directories to the repository",
	Long:   `Push files and directories to the repository.`,
	PreRun: prePush,
	Run:    runPush,
}

func initPushCmd() {
	pushCmd.Flags().StringVarP(&settings.Cred.Password, "password", "", settings.GetPassword(), "the user password")
	pushCmd.Flags().StringVarP(&settings.Cred.KeyFile, "key-file", "", settings.GetKeyFile(), "the user provided key file")
	pushCmd.Flags().StringVarP(&settings.Saved.RootPath, "repo", "r", "", "the repository")
	pushCmd.Flags().BoolVarP(&settings.Force, "force", "f", false, "force")
	pushCmd.Flags().BoolVarP(&settings.Verbose, "verbose", "v", false, "force")
	pushCmd.Flags().BoolVarP(&settings.DryRun, "dry-run", "", false, "dry run")
	pushCmd.Flags().BoolVarP(&settings.All, "all", "a", false, "push all changes, including deletes")

	RootCmd.AddCommand(pushCmd)
}

func prePush(cmd *cobra.Command, args []string) {
}

func runPush(cmd *cobra.Command, args []string) {
	err := settings.LoadRepoConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	settings.CreateKey()
	err = settings.CreateKey()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	var src string
	var dest string

	if len(args) < 1 {
		// Done
		os.Exit(0)
	} else if len(args) == 1 {
		src = args[0]
		dest = "/"
	} else {
		src = args[0]
		dest = args[1]
	}

	err = sotlib.Push(&settings, src, dest)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
