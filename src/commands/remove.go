package commands

import (
	"fmt"
	"os"
	"sotlib"

	"github.com/spf13/cobra"
)

var removeCmd = &cobra.Command{
	Use:     "rm",
	Aliases: []string{"remove"},
	Short:   "removes from the repository",
	Long:    `Removes files and directories from the repository.`,
	PreRun:  preRemove,
	Run:     runRemove,
}

func initRemoveCmd() {
	removeCmd.Flags().StringVarP(&settings.Cred.Password, "password", "", settings.GetPassword(), "the user password")
	removeCmd.Flags().StringVarP(&settings.Cred.KeyFile, "key-file", "", settings.GetKeyFile(), "the user provided key file")
	removeCmd.Flags().StringVarP(&settings.Saved.RootPath, "repo", "r", "", "the repository")
	removeCmd.Flags().BoolVarP(&settings.Verbose, "verbose", "v", false, "force")

	RootCmd.AddCommand(removeCmd)
}

func preRemove(cmd *cobra.Command, args []string) {
}

func runRemove(cmd *cobra.Command, args []string) {
	err := settings.LoadRepoConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	settings.CreateKey()
	err = settings.CreateKey()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	if len(args) < 1 {
		// Done
		os.Exit(0)
	}

	err = sotlib.Remove(&settings, args)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
