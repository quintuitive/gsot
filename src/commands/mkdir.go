package commands

import (
	"fmt"
	"os"
	"sotlib"

	"github.com/spf13/cobra"
)

var mkdirCmd = &cobra.Command{
	Use:    "mkdir",
	Short:  "create a new directory in the sot repository",
	Long:   `Similar to the 'mkdir' utility. Operates over a sot tree.`,
	PreRun: preMkdir,
	Run:    runMkdir,
}

func initMkdirCmd() {
	mkdirCmd.Flags().StringVarP(&settings.Cred.Password, "password", "", settings.GetPassword(), "the user password")
	mkdirCmd.Flags().StringVarP(&settings.Cred.KeyFile, "key-file", "", settings.GetKeyFile(), "the user provided key file")
	mkdirCmd.Flags().BoolVarP(&settings.CreateParents, "parents", "p", false, "make parent directories as needed")
	mkdirCmd.Flags().StringVarP(&settings.Saved.RootPath, "repo", "r", "", "the repository")

	RootCmd.AddCommand(mkdirCmd)
}

func preMkdir(cmd *cobra.Command, args []string) {
}

func runMkdir(cmd *cobra.Command, args []string) {
	err := settings.LoadRepoConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	settings.CreateKey()
	err = settings.CreateKey()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	err = sotlib.Mkdir(&settings, args)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
