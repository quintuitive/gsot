package commands

import (
	"fmt"
	"os"
	"sotlib"
	"strings"

	"github.com/spf13/cobra"
)

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "initializes a sot repository",
	Long: `Initializes a sot repository in the specified folder, or (if none was
specified), into the current directory. The target folder must be empty.`,
	PreRun: preInit,
	Run:    runInit,
}

var initCiphers string

func initInitCmd() {
	initCmd.Flags().StringVarP(&settings.Cred.Password, "password", "", settings.GetPassword(), "the user password")
	initCmd.Flags().StringVarP(&settings.Cred.KeyFile, "key-file", "", settings.GetKeyFile(), "the user provided key file")
	initCmd.Flags().IntVarP(&settings.RepoCfg.MinFileSize, "min-file-size", "", 1024, "minimum file size after compression")
	initCmd.Flags().IntVarP(&settings.RepoCfg.CompressionLevel, "compression-level", "", 5, "the compression level")
	initCmd.Flags().StringVarP(&initCiphers, "ciphers", "c", "aes", "the encryption ciphers used")

	RootCmd.AddCommand(initCmd)
}

func preInit(cmd *cobra.Command, args []string) {
	// The repository settings are initialized from cached values. Reset to the defaults.
	// settings.RepoCfg.Init()
}

func runInit(cmd *cobra.Command, args []string) {

	if len(initCiphers) > 0 {
		settings.RepoCfg.Ciphers = []string{}
		cc := strings.Split(initCiphers, ",")
		noCiphers := false
		for _, vv := range cc {
			vv = strings.TrimSpace(vv)
			switch vv {
			case "aes":
				settings.RepoCfg.Ciphers = append(settings.RepoCfg.Ciphers, vv)
			case "twofish":
				settings.RepoCfg.Ciphers = append(settings.RepoCfg.Ciphers, vv)
			case "none":
				noCiphers = true
				break
			default:
				fmt.Println("Unsupported cipher '" + vv + "'")
				os.Exit(-1)
			}
		}
		if noCiphers {
			settings.RepoCfg.Ciphers = []string{}
		}
	}

	err := settings.CreateKey()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	if len(args) < 1 {
		rp, err := os.Getwd()
		if err != nil {
			fmt.Println(err)
			os.Exit(-1)
		}
		settings.Saved.RootPath = sotlib.CanonicalPath(rp)
	} else {
		settings.Saved.RootPath = sotlib.CanonicalPath(args[len(args)-1])
	}

	err = sotlib.Init(&settings)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
