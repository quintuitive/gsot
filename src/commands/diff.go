package commands

import (
	"fmt"
	"os"
	"sotlib"

	"github.com/spf13/cobra"
)

var diffCmd = &cobra.Command{
	Use:     "diff",
	Aliases: []string{"diff"},
	Short:   "compare the content of a local path vs the content of a repository path",
	Long:    `Similar to the 'diff' utility. Operates over a sot tree.`,
	PreRun:  preDiff,
	Run:     runDiff,
}

func initDiffCmd() {
	diffCmd.Flags().StringVarP(&settings.Cred.Password, "password", "", settings.GetPassword(), "the user password")
	diffCmd.Flags().StringVarP(&settings.Cred.KeyFile, "key-file", "", settings.GetKeyFile(), "the user provided key file")
	diffCmd.Flags().StringVarP(&settings.Saved.RootPath, "repo", "r", "", "the repository")

	RootCmd.AddCommand(diffCmd)
}

func preDiff(cmd *cobra.Command, args []string) {
}

func runDiff(cmd *cobra.Command, args []string) {

	err := settings.LoadRepoConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	settings.CreateKey()
	err = settings.CreateKey()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	var src string
	var dest string

	if len(args) < 1 {
		// Done
		os.Exit(0)
	} else if len(args) > 2 {
		fmt.Println("The diff command takes maxium two arguments: source [local path] and destination [repository path]")
		os.Exit(-1)
	} else if len(args) == 1 {
		dest = "/"
	} else {
		dest = args[len(args)-1]
		src = args[len(args)-2]
	}

	err = sotlib.Diff(&settings, src, dest)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
