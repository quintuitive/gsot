package commands

import (
	"fmt"
	"os"
	"sotlib"

	"github.com/spf13/cobra"
)

var pullCmd = &cobra.Command{
	Use:   "pull",
	Short: "pulls files and directories from the repository",
	Long:  `Pulls files and directories from the repository.`,
	Run:   runPull,
}

func initPullCmd() {
	pullCmd.Flags().StringVarP(&settings.Cred.Password, "password", "", settings.GetPassword(), "the user password")
	pullCmd.Flags().StringVarP(&settings.Cred.KeyFile, "key-file", "", settings.GetKeyFile(), "the user provided key file")
	pullCmd.Flags().StringVarP(&settings.Saved.RootPath, "repo", "r", "", "the repository")
	pullCmd.Flags().BoolVarP(&settings.Force, "force", "f", false, "force")
	pullCmd.Flags().BoolVarP(&settings.Verbose, "verbose", "v", false, "force")
	pullCmd.Flags().BoolVarP(&settings.DryRun, "dry-run", "", false, "dry run")

	RootCmd.AddCommand(pullCmd)
}

func runPull(cmd *cobra.Command, args []string) {
	err := settings.LoadRepoConfig()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	settings.CreateKey()
	err = settings.CreateKey()
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	var dest string
	var src string

	if len(args) < 1 {
		// Done
		os.Exit(0)
	} else if len(args) == 1 {
		src = args[0]
		dest = "/"
	} else if len(args)  == 2 {
		src = args[0]
		dest = args[1]
	} else {
		os.Exit(-1)
	}

	err = sotlib.Pull(&settings, src, dest)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}
