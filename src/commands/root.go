package commands

import (
	"sotlib"

	"github.com/spf13/cobra"
)

var settings = sotlib.InitAndLoadSettings()

var RootCmd = &cobra.Command{
	Use:   "sot command [arguments]",
	Short: "A cloud storage/backup utility allowing light remote access to files",
	Long: `Simple Object Tree (sot) is a tool to store files efficiently (via compression)
and safely (via encryption) in the cloud allowing a light-weight access to them.`,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

func init() {

	initPushCmd()
	initPullCmd()
	initInitCmd()
	initListCmd()
	initMkdirCmd()
	initRemoveCmd()
	initVerifyCmd()
	initDiffCmd()
	initKeygenCmd()

	// Initialize using the defaults
	settings.Init()

	// Load from files
	settings.LoadCredentials()
	settings.LoadSaved()

	RootCmd.SetUsageTemplate(`
Usage:    
    {{if .HasAvailableSubCommands}}
  {{ .CommandPath}} [command] [arguments]{{end}}{{ if .HasAvailableSubCommands}}
  
The commands are:
{{range .Commands}}{{if .IsAvailableCommand}}
  {{rpad .Name .NamePadding }} {{.Short}}{{end}}{{end}}{{end}}{{if .HasHelpSubCommands}}
  
Additional help topics:{{range .Commands}}{{if .IsHelpCommand}}
  {{rpad .CommandPath .CommandPathPadding}} {{.Short}}{{end}}{{end}}{{end}}{{ if .HasAvailableSubCommands }}
  
Use "{{.CommandPath}} [command] --help" for more information about a command.{{end}}
`)

	RootCmd.SetHelpTemplate(`
{{with or .Long .Short }}{{. | trim}}
{{end}}{{if or .Runnable .HasSubCommands}}{{.UsageString}}{{end}}
`)
}
