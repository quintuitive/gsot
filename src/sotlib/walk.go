package sotlib

import (
	"errors"
	"path"

	"github.com/oleiade/lane"
)

type WalkFunc func(path string, info []FileInfo, err error) error

var SkipDir = errors.New("skip this directory")

func Walk(settings *Settings, root string, walkFn WalkFunc) error {

	dirQueue := lane.NewQueue()
	dirQueue.Append(root)

	for !dirQueue.Empty() {
		curPath := dirQueue.Pop().(string)

		fi, err := Stat(settings, curPath)
		curDir, _, dirErr := OpenDir(settings, curPath, false, false)

		if err == nil {
			err = dirErr
		}

		if err != nil {
			return err
		}

		fis := make([]FileInfo, 1)
		fis[0] = fi

		// fmt.Printf("Walk: curPath = %v, fi = %v\n", curPath, fi)
		walkErr := walkFn(curPath, fis, err)
		if walkErr == SkipDir {
		} else if walkErr != nil {
			return walkErr
		} else {
			for kk, vv := range curDir.GetEntries() {
				if vv.IsDir() {
					dirQueue.Append(path.Join(curPath, kk))
				} else {
					efis := make([]FileInfo, len(vv.Versions))
					for vid := 0; vid < len(vv.Versions); vid++ {
						efis[vid] = vv.FileInfo(kk, vid)
					}

					// fmt.Printf("Walk: kk = %v, vv = %v, efi = %v\n", kk, vv, efi)
					walkErr = walkFn(path.Join(curPath, kk), efis, nil)
					if walkErr != nil {
						return walkErr
					}
				}
			}
		}

	}

	return nil
}
