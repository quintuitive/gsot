package sotlib

import (
	"os"
	"os/user"
	"path"

	"github.com/BurntSushi/toml"
)

type Credentials struct {
	Password string
	KeyFile  string
}

func (cred *Credentials) Load() {
	usr, err := user.Current()
	if err != nil {
		return
	}

	pp := path.Join(usr.HomeDir, ".sot", "credentials.toml")

	if _, err := os.Stat(pp); err == nil {
		toml.DecodeFile(pp, &cred)
	}
}

func (cred *Credentials) Init() {
}
