package sotlib

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/oleiade/lane"
)

type DirPair struct {
	src  string
	dest string
}

func Diff(settings *Settings, src string, dest string) error {
	// Accept only UNIX style paths as destination
	if dest != filepath.ToSlash(dest) {
		return errors.New("The destionation path [" + dest + "] doesn't seem to be a valid UNIX-style path.")
	}

	src = path.Clean(filepath.ToSlash(src))

	fi, err := os.Stat(src)
	if err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}

	queue := lane.NewQueue()

	srcIsDir := fi.IsDir()

	if srcIsDir {
		queue.Append(DirPair{src, dest})
	} else {
		pp := path.Clean(dest)
		destDir := path.Dir(pp)
		dir, _, err := OpenDir(settings, destDir, false, false)
		if err != nil {
			return err
		}

		// Lookup the last path element into the directory
		destFile := path.Base(pp)
		de, ok := dir.LookupEntry(destFile)
		if ok {
			if de.IsDir() {
				// The last element is a directory. Copy the source (a directory) into
				// the already opened directory.
				fmt.Println(src + " is a file, while " + dest + " is a directory")
			} else {
				err, ds := de.Diff(src)
				if err != nil {
					return err
				}

				strOut := ""
				if ds.Newer != 0 || ds.SizeDiff || ds.ChecksumDiff {
					strOut = src + " and " + dest + " seem to differ:"
					addComma := false
					if ds.Newer > 0 {
						strOut += " the repository version is newer"
						addComma = true
					} else if ds.Newer < 0 {
						strOut += " the local version is newer"
						addComma = true
					}
					if ds.SizeDiff {
						if addComma {
							strOut += ","
						}
						strOut += " the size is different"
					}
					if ds.ChecksumDiff {
						if addComma {
							strOut += ","
						}
						strOut += " the content is different"
					}
					fmt.Println(strOut)
				}
			}
		} else {
			fmt.Println(src + " exists only in the local file system")
		}
	}

	localUuids := make(map[string]bool)

	// Process the queue
	for !queue.Empty() {
		// Pop the head of the queue
		dp := queue.Dequeue()

		destPath := dp.(DirPair).dest
		srcPath := dp.(DirPair).src

		// fmt.Print(destPath + "\n" + srcPath + "\n")

		dir, _, err := OpenDir(settings, path.Dir(destPath), false, false)
		if err != nil {
			return err
		}

		baseDestPath := path.Base(destPath)
		de, ok := dir.LookupEntry(baseDestPath)
		if !ok {
			fmt.Println(srcPath + " exists only in the local file system")
			continue
		}

		if de.IsFile() {
			fmt.Println(srcPath + " is a directory, " + destPath + " is a file")
			localUuids[de.Uuid()] = true
			continue
		} else {
			localUuids[de.Uuid()] = false
		}

		// Enter the repository sub directory
		dir.ChdirDown(baseDestPath)

		// Process all directory entries. Files are added to the repo directly,
		// sub directories are queued for further processing.
		realDir, err := os.Open(srcPath)
		if err != nil {
			return err
		}

		rdes, err := realDir.Readdir(-1)
		if err != nil {
			return err
		}

		for _, rde := range rdes {
			// fmt.Println("Processing: " + rde.Name())
			if rde.IsDir() {
				fullSrcPath := path.Join(dp.(DirPair).src, rde.Name())
				queue.Append(DirPair{fullSrcPath, path.Join(destPath, rde.Name())})
			} else {
				// A file - diff it

				fullSrcPath := path.Join(dp.(DirPair).src, rde.Name())
				fullDestPath := path.Join(dp.(DirPair).dest, rde.Name())

				de, ok := dir.LookupEntry(rde.Name())
				if !ok {
					fmt.Println(fullSrcPath + " exists only in the local file system")
					continue
				}

				if de.IsDir() {
					fmt.Println(fullSrcPath + " is a file, " + fullDestPath + " is a directory")
					localUuids[de.Uuid()] = true
					continue
				} else {
					localUuids[de.Uuid()] = false
				}

				err, ds := de.Diff(fullSrcPath)
				if err != nil {
					return err
				}

				strOut := ""
				if ds.Newer != 0 || ds.SizeDiff || ds.ChecksumDiff {
					strOut = fullSrcPath + " and " + fullDestPath + " seem to differ:"
					addComma := false
					if ds.Newer > 0 {
						strOut += " the repository version is newer"
						addComma = true
					} else if ds.Newer < 0 {
						strOut += " the local version is newer"
						addComma = true
					}
					if ds.SizeDiff {
						if addComma {
							strOut += ","
						}
						strOut += " the size is different"
					}
					if ds.ChecksumDiff {
						if addComma {
							strOut += ","
						}
						strOut += " the content is different"
					}
					fmt.Println(strOut)
				}
			}
		}
	}

	if srcIsDir {
		// Scan the object tree
		callBack := func(ss string, fis []FileInfo, err error) error {
			// fmt.Printf("path: %s, entries: %d\n", ss, len(fis))
			_, ok := localUuids[fis[0].Uuid()]
			if !ok {
				var typeStr string
				if fis[0].IsDir() {
					typeStr = "directory"
				} else {
					typeStr = "file"
				}
				fmt.Println(ss + " [a " + typeStr + "] exists only in the repository")
				if fis[0].IsDir() {
					return SkipDir
				}
			}
			return nil
		}

		fi, err := Stat(settings, dest)
		if err == nil && fi != nil {
			err := Walk(settings, dest, callBack)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func Push(settings *Settings, src string, dest string) error {

	// Accept only UNIX style paths as destination
	if dest != filepath.ToSlash(dest) {
		return errors.New("The destionation path [" + dest + "] doesn't seem to be a valid UNIX-style path.")
	}

    src = filepath.ToSlash(src)
    srcEndWithSlash := src[len(src)-1] == '/'
    src = path.Clean(src)

    fi, err := os.Stat(src)
    if err != nil {
        fmt.Println(err)
        os.Exit(-1)
    }

    queue := lane.NewQueue()

    srcIsDir := fi.IsDir()

    var fullDestPath string

    if fi.IsDir() {

        // src is a folder, let's define the desired behaviour, following
        // the design of rsync:
        //
        // sot pull /user/documents/ c:/home/user
        //    Content of /user/documents goes into c:/home/user
        //
        // sot pull /user/documents c:/home/user
        //    Content of /user/documents goes into c:/home/user/documents
        //
        // Notice the difference caused by the presence/lack of the final slash
        // in the source (documents/ vs just documents).

        // We need to figure out whether the destionation exists, and if so, what
        // is the destionation's type [file vs folder].

        // Open the destination except the last path element
        pp := path.Clean(dest)
        destDir := path.Dir(pp)
        dir, _, err := OpenDir(settings, destDir, false, false)
        if err != nil {
            return err
        }
        defer dir.WriteIfNeeded()

        // Lookup the last path element into the directory
        destFile := path.Base(pp)
        de, ok := dir.LookupEntry(destFile)
        if ok {
            if de.IsDir() {
                // The last element is a directory. Copy the source (a directory) into
                // the already opened directory.

                var fullDestPath string
                if srcEndWithSlash {
                    fullDestPath = dest
                } else {
                    fullDestPath = path.Join(dest, path.Base(src))
                }

                queue.Append(DirPair{src, fullDestPath})
                Println(settings, "Queueing directory "+src+" [target: "+fullDestPath+"]")
            } else {
                // The last element is a file
                if !settings.Force {
                    return errors.New("The directory '" + src + "' cannot be copied - a file with the same name exists.")
                }

                // Remove the file and copy the directory under the new name
                err = dir.RemoveFileEntry(destFile, de)
                if err != nil {
                    return err
                }
                dir.Write()

                var fullDestPath string
                if srcEndWithSlash {
                    fullDestPath = dest
                } else {
                    fullDestPath = path.Join(dest, path.Base(src))
                }

                queue.Append(DirPair{src, fullDestPath})
                Println(settings, "Queueing directory "+src+" [target: "+fullDestPath+"]")
            }
        } else {
            // The entry doesn't exist
            var fullDestPath string
            if srcEndWithSlash {
                fullDestPath = dest
            } else {
                fullDestPath = path.Join(dest, path.Base(src))
            }
            queue.Append(DirPair{src, fullDestPath})
            Println(settings, "Queueing directory "+src+" [target: "+fullDestPath+"]")
        }

        // Process the queue
        for !queue.Empty() {
            // Pop the head of the queue
            dp := queue.Dequeue()

            // Process all directory entries. Files are added to the repo directly,
            // sub directories are queued for further processing.
            realDir, err := os.Open(dp.(DirPair).src)
            if err != nil {
                return err
            }

            rdes, err := realDir.Readdir(-1)
            if err != nil {
                return err
            }

            // fmt.Println("\nProcessing pair: src = " + dp.(DirPair).src + "; dest = " + dp.(DirPair).dest)

            destPath := dp.(DirPair).dest
            srcPath := dp.(DirPair).src

            // fmt.Println(srcPath + " ---> " + destPath)

            dir, _, err := OpenDir(settings, path.Dir(destPath), false, false)
            if err != nil {
                return err
            }
            defer dir.WriteIfNeeded()

            if len(destPath) > 0 && destPath != "/" {
                baseDestPath := path.Base(destPath)
                if _, ok := dir.LookupEntry(baseDestPath); ok {
                } else {
                    err = dir.AddDir(baseDestPath, srcPath)
                    if err != nil {
                        return err
                    }
                }

                dir.ChdirDown(baseDestPath)
            }

            for _, rde := range rdes {
                if rde.IsDir() {
                    srcPath := path.Join(dp.(DirPair).src, rde.Name())
                    queue.Append(DirPair{srcPath, path.Join(dp.(DirPair).dest, rde.Name())})
                    Println(settings, "Queueing directory "+path.Join(dp.(DirPair).src, rde.Name())+" [target: "+path.Join(dp.(DirPair).dest, rde.Name())+"]")
                } else {
                    // A file - copy it
                    err, added := dir.AddFile(path.Join(dp.(DirPair).src, rde.Name()), rde.Name(), false)
                    if err != nil {
                        return err
                    }
                    if added {
                        Println(settings, "Added "+path.Join(dp.(DirPair).src, rde.Name())+" as "+path.Join(dp.(DirPair).dest, rde.Name()))
                    } else {
                        Println(settings, "Skipped "+path.Join(dp.(DirPair).src, rde.Name())+". The repository file "+path.Join(dp.(DirPair).dest, rde.Name())+" is the same.")
                    }
                }
            }

            dir.Write()
        }
    } else {

        // The source is a file, let's define the desired behaviour:
        //
        // 		sot /home/user/documents/report.doc /documents/reports
        //
        // In this example the behaviour depends whether "reports" is
        // a file or a directory. If it's a directory, then report.doc
        // is copied to /documents/reports/report.doc. Otherwise,
        // report.doc is copied to /documents/reports.

        if strings.HasSuffix(dest, "/") || strings.HasSuffix(dest, "/.") || strings.HasSuffix(dest, "/..") {
            // The destination must be a directory. Open it and check.
            destDir := path.Clean(dest)
            dir, isFile, err := OpenDir(settings, destDir, false, false)
            if err != nil {
                return err
            }

            if isFile {
                return errors.New("The destination path " + destDir + " has non-directory components in it.")
            }

            // Copy the source (a file) into the already opened directory. The name is retrieved from the source.
            err, added := dir.AddFile(src, "", false)
            if err != nil {
                return err
            }

            if added {
                Println(settings, "Added "+src+" to directory "+destDir)
            } else {
                Println(settings, "Skipped "+src+". The repository file in "+destDir+" is the same.")
            }

            dir.WriteIfNeeded()
        } else {
            // Open the destination except the last path element
            pp := path.Clean(dest)
            destDir := path.Dir(pp)
            dir, _, err := OpenDir(settings, destDir, false, false)
            if err != nil {
                return err
            }

            // Lookup the last path element into the directory
            destFile := path.Base(pp)
            de, ok := dir.LookupEntry(destFile)
            if ok {
                if de.IsDir() {
                    // The last element is a directory. Copy the source (a file) into
                    // the already opened directory. The name is retrieved from the source.
                    err = dir.ChdirDown(destFile)
                    if err != nil {
                        return err
                    }

                    err, added := dir.AddFile(src, "", false)
                    if err != nil {
                        return err
                    }

                    if added {
                        Println(settings, "Added "+src+" as "+path.Join(pp, path.Base(src)))
                    } else {
                        Println(settings, "Skipped "+src+". The repository file in "+path.Join(pp, path.Base(src))+" is the same.")
                    }
                } else {
                    Println(settings, "Copying "+src+" to "+pp)

                    // The last element is a file. Overwrite it.
                    err, added := dir.AddFile(src, destFile, false)
                    if err != nil {
                        return err
                    }

                    if added {
                        Println(settings, "Added "+src+" as "+pp)
                    } else {
                        Println(settings, "Skipped "+src+". The repository file in "+pp+" is the same.")
                    }
                }
            } else {
                // The entry doesn't exist. Add a file.
                err, _ = dir.AddFile(src, destFile, false)
                if err != nil {
                    return err
                }

                Println(settings, "Added "+src+" as "+destFile)
            }
            dir.WriteIfNeeded()
        }
    }

    if settings.All && srcIsDir {
        diffs, err := DiffEntries(settings, src, fullDestPath)
        if err != nil {
            return err
        }

        var removeList []string

        for _, vv := range diffs {
            if len(vv.repoPath) > 0 && (len(vv.localPath) == 0 || vv.localType == 0) {
                removeList = append(removeList, vv.repoPath)
            }
        }

        Remove(settings, removeList)
    }


	return nil
}

func Mkdir(settings *Settings, srcs []string) error {
	for ii := 0; ii < len(srcs); ii++ {
		srcs[ii] = path.Clean(filepath.ToSlash(srcs[ii]))
        if srcs[ii][0] != '/' {
            srcs[ii] = "/" + srcs[ii]
        }
	}

	for _, pp := range srcs {
		if settings.CreateParents {
			dir, _, err := OpenDir(settings, pp, true, false)
			if err != nil {
				return err
			}

			dir.Write()
		} else {
			// Open the parent directory
			parentPath := path.Dir(pp)
			dir, ff, err := OpenDir(settings, parentPath, false, false)
			if err != nil {
				return err
			}

			if ff {
				fmt.Println("The path [" + pp + "] has non-directory components in it.")
				os.Exit(-1)
			}

			err = dir.AddDir(path.Base(pp), "")
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func writePermissionString(perm uint64, buffer *bytes.Buffer) {
	var mask uint64 = 0x100
	letters := "rwx"
	for ii := 0; ii < 9; ii++ {
		if (perm & mask) == mask {
			buffer.WriteByte(letters[ii%3])
		} else {
			buffer.WriteByte('-')
		}
		mask = mask >> 1
	}
}

func List(settings *Settings, thepath string) error {

	// Use UNIX style paths
	thepath = path.Clean(filepath.ToSlash(thepath))

	dir, isFile, err := OpenDir(settings, thepath, false, true)
	if err != nil {
		return err
	}

	if isFile {

	} else {
		entryNames := make([]string, dir.GetNumEntries())

		// nameCol := make([]string, dir.GetNumEntries())
		// sizeCol := make([]string, dir.GetNumEntries())
		// mtimeCol := make([]string, dir.GetNumEntries())
		// attrCol := make([]string, dir.GetNumEntries())
		var nameCol []string
		var sizeCol []string
		var mtimeCol []string
		var attrCol []string
		var versionCol []string

		ii := 0
		for kk, _ := range dir.GetEntries() {
			entryNames[ii] = kk
			ii++
		}

		sort.Strings(entryNames)

		maxSizeWidth := 0
		maxTimeWidth := 0

		nameId := 0

		for _, vv := range entryNames {
			de, _ := dir.LookupEntry(vv)
			var buffer bytes.Buffer
			if de.IsDir() {
				buffer.WriteString("d")
				sizeCol = append(sizeCol, "---")
			} else {
				buffer.WriteString("-")
				sizeCol = append(sizeCol, strconv.FormatInt(de.Size(), 10))
			}

			if len(sizeCol[nameId]) > maxSizeWidth {
				maxSizeWidth = len(sizeCol[nameId])
			}
			writePermissionString(de.Attributes(), &buffer)
			attrCol = append(attrCol, buffer.String())

			mtimeCol = append(mtimeCol, time.Unix(de.ModTime(), 0).Format("Jan _2 15:04"))
			if len(mtimeCol[nameId]) > maxTimeWidth {
				maxTimeWidth = len(mtimeCol[nameId])
			}

			versionCol = append(versionCol, " ")

			nameCol = append(nameCol, vv)
			nameId++

			if settings.Versions && len(de.Versions) > 1 {
				vid := int(de.Latest) - 1
				for true {
					if vid < 0 {
						vid = len(de.Versions) - 1
					}
					if vid == int(de.Latest) {
						break
					}

					ve := de.Versions[vid]

					buffer.Reset()
					buffer.WriteString("-")
					writePermissionString(ve.Attributes, &buffer)

					attrCol = append(attrCol, buffer.String())
					sizeCol = append(sizeCol, strconv.FormatInt(ve.Size, 10))
					mtimeCol = append(mtimeCol, time.Unix(ve.ModTime, 0).Format("Jan _2 15:04"))

					if len(sizeCol[nameId]) > maxSizeWidth {
						maxSizeWidth = len(sizeCol[nameId])
					}

					if len(mtimeCol[nameId]) > maxTimeWidth {
						maxTimeWidth = len(mtimeCol[nameId])
					}

					versionCol = append(versionCol, "*")

					nameCol = append(nameCol, vv)
					nameId++

					vid++
				}
			}
		}

		for kk, vv := range nameCol {
			var line bytes.Buffer
			if settings.LongFormat || settings.Versions {
				line.WriteString(attrCol[kk])
				line.WriteByte(' ')
				for ii = len(sizeCol[kk]); ii < maxSizeWidth; ii++ {
					line.WriteByte(' ')
				}
				line.WriteString(sizeCol[kk])
				line.WriteByte(' ')
				line.WriteString(mtimeCol[kk])
				line.WriteByte(' ')
				if settings.Versions {
					line.WriteString(versionCol[kk])
					line.WriteByte(' ')
				}
			}
			line.WriteString(vv)

			fmt.Println(line.String())
		}
	}

	return nil
}

func Pull(settings *Settings, src string, dest string) error {

	dest = path.Clean(filepath.ToSlash(dest))
    src = filepath.ToSlash(src)
    srcEndsWithSlash := src[len(src) - 1] ==  '/'
	src = path.Clean(src)

	queue := lane.NewQueue()

	fi, err := Stat(settings, src)
	if err != nil {
		return err
	}

	if fi == nil {
		return errors.New("Failed to open '" + src + "'")
	}

	if fi.IsDir() {
		// src is a folder, let's define the desired behaviour, following
        // the design of rsync:
        //
        // sot pull /user/documents/ c:/home/user
        //    Content of /user/documents goes into c:/home/user
		//
		// sot pull /user/documents c:/home/user
		//    Content of /user/documents goes into c:/home/user/documents
        //
        // Notice the difference caused by the presence/lack of the final slash
        // in the source (documents/ vs just documents).

		fileName := path.Base(src)

		destPath := dest
		if fileName != "" && !srcEndsWithSlash {
			destPath = path.Join(destPath, fileName)
			os.Mkdir(destPath, os.ModePerm)
		}

		queue.Append(DirPair{src, destPath})
	} else {
		srcDir, isFile, err := OpenDir(settings, src, false, true)
		if err != nil {
			return err
		}

		if !isFile {
			return errors.New("The source [" + src + "] was expected to be a file, but it is a directory.")
		}

		fi, err := os.Stat(dest)
		if err != nil {
			return err
		}

		fileName := path.Base(src)

		destPath := dest
		if fi.IsDir() {
			destPath = path.Join(destPath, fileName)
		}

		got, err := srcDir.GetFile(fileName, destPath, false)
		if err != nil {
			return err
		}
		if got {
			Println(settings, "Copied "+src+" to "+destPath)
		} else {
			Println(settings, "Skipped "+src+". The local file is the same as the repository file ["+destPath+"].")
		}
	}

	// Process the queue
	for !queue.Empty() {
		// Pop the head of the queue
		dp := queue.Dequeue()

		srcDir, _, err := OpenDir(settings, dp.(DirPair).src, false, false)
		if err != nil {
			return err
		}

		for name, de := range srcDir.GetEntries() {
			if de.IsDir() {
				destDirPath := path.Join(dp.(DirPair).dest, name)
				err = os.Mkdir(destDirPath, os.FileMode(de.Attributes()))

				srcDirPath := path.Join(dp.(DirPair).src, name)
				queue.Append(DirPair{srcDirPath, destDirPath})
				Println(settings, "Queueing directory "+srcDirPath+" [target: "+destDirPath+"]")
			} else {
				got, err := srcDir.GetFile(name, path.Join(dp.(DirPair).dest, name), false)
				if err != nil {
					return err
				}
				if got {
					Println(settings, "Copied "+path.Join(dp.(DirPair).src, name)+" to "+path.Join(dp.(DirPair).dest, name))
				} else {
					Println(settings, "Skipped "+path.Join(dp.(DirPair).src, name)+". The local file is the same as the repository file ["+path.Join(dp.(DirPair).dest, name)+"].")
				}
			}
		}
	}

	return nil
}

func Stat(settings *Settings, thepath string) (FileInfo, error) {

	thepath = path.Clean(filepath.ToSlash(thepath))

	if !strings.HasPrefix(thepath, "/") {
		thepath = "/" + thepath
	}

	// The root path requires special handling
	if thepath == "/" {
		fi, err := os.Stat(path.Join(settings.GetRootPath(), "objects", RootPath))
		if err != nil {
			return nil, err
		}
		return &fileInfo{"/", fi.Size(), fi.Mode(), fi.ModTime(), true, RootUuid, nil}, nil
	}

	parentPath := path.Dir(thepath)
	if parentPath == "" {
		parentPath = "/"
	}

	dir, _, err := OpenDir(settings, parentPath, false, false)
	if err != nil {
		return nil, err
	}

	name := path.Base(thepath)

	de, ok := dir.LookupEntry(name)
	if !ok {
		return nil, nil
	}

	return de.LatestFileInfo(name), nil
}

func Init(settings *Settings) error {
	bb, err := isEmpty(settings.GetRootPath())

	if err != nil {
		return errors.New("Failed to read the directory " + settings.GetRootPath())
	}

	if !bb {
		return errors.New("A new repository is created only in an empty directory and " + settings.GetRootPath() + " is not empty\n")
	}

	// Write the repository configuration
	err = settings.WriteRepoConfig()
	if err != nil {
		return err
	}

	// Create the directory structure
	err = os.Mkdir(path.Join(settings.GetRootPath(), "objects"), os.ModePerm)
	if err != nil {
		return err
	}

	err = os.Mkdir(path.Join(settings.GetRootPath(), "objects", RootDir), os.ModePerm)
	if err != nil {
		return err
	}

	dir := NewRootDir(settings)
	err = dir.Write()
	if err != nil {
		return err
	}

	settings.WriteSaved()

	return nil
}

func isEmpty(name string) (bool, error) {
	f, err := os.Open(name)
	if err != nil {
		return false, err
	}
	defer f.Close()

	_, err = f.Readdirnames(1)
	if err == io.EOF {
		return true, nil
	}
	return false, err
}

func Remove(settings *Settings, paths []string) error {
	for _, pp := range paths {
		pp = path.Clean(pp)
		fi, err := Stat(settings, pp)
		if err != nil {
			return err
		}

		if fi.IsDir() {
			scanStack := lane.NewStack()
			scanStack.Push(pp)

			stack := lane.NewStack()
			for !scanStack.Empty() {
				top := scanStack.Head().(string)
				scanStack.Pop()
				dir, isFile, err := OpenDir(settings, top, false, false)
				if err != nil {
					return err
				}

				if isFile {
					return errors.New("The destination path [" + top + "] has non-directory components in it.")
				}

				stack.Push(top)
				for kk, vv := range dir.GetEntries() {
					if vv.IsDir() {
						scanStack.Push(path.Join(top, kk))
					}
				}
			}

			for !stack.Empty() {
				top := stack.Head().(string)
				stack.Pop()
				dir, isFile, err := OpenDir(settings, top, false, false)
				if err != nil {
					return err
				}

				if isFile {
					return errors.New("Unexpected file [" + top + "] found where a directory was expected.")
				}

				err = dir.RemoveAllEntries()
				if err != nil {
					return err
				}

				parentPath := path.Dir(top)
				parentDir, _, err := OpenDir(settings, parentPath, false, false)
				if err != nil {
					return err
				}

				parentDir.RemoveEntry(path.Base(top))
				parentDir.WriteIfNeeded()

			}
		} else {
			dir, _, err := OpenDir(settings, pp, false, true)
			if err != nil {
				return err
			}

			err = dir.RemoveEntry(path.Base(pp))
			if err != nil {
				return err
			}

			dir.WriteIfNeeded()
		}
	}

	return nil
}

func Verify(settings *Settings) error {
	uuids, err := ListUuids(settings)
	if err != nil {
		return err
	}

	/*
		for ii, vv := range uuids {
			fmt.Printf("%d: %s\n", ii, vv)
		}
	*/

	fileInfos, err := ListObjects(settings, "/")
	if err != nil {
		return err
	}

	/*
		for ii, fi := range fileInfos {
			fmt.Printf("%d: %s - %s\n", ii, fi.Name(), fi.Uuid())
		}
	*/

	uuidMap := make(map[string]bool)
	for _, vv := range uuids {
		uuidMap[vv] = false
	}

	fileInfoMap := make(map[string]bool)
	for _, vv := range fileInfos {
		fileInfoMap[vv.Uuid()] = false
	}

	for kk, _ := range uuidMap {
		_, uuidMap[kk] = fileInfoMap[kk]
	}

	for kk, _ := range fileInfoMap {
		_, fileInfoMap[kk] = uuidMap[kk]
	}

	success := true
	for kk, vv := range uuidMap {
		if !vv {
			fmt.Printf("Error: object %s not found in the repository.\n", kk)
			success = false
		}
	}

	for kk, vv := range fileInfoMap {
		if !vv {
			fmt.Printf("Error: repository object %s not found in the file system.\n", kk)
			success = false
		}
	}

	if success {
		fmt.Printf("Verification was successful. No errors were found.\n")
	}

	return nil
}
