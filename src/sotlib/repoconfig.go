package sotlib

import (
	"os"

	"github.com/BurntSushi/toml"
)

type RepoConfig struct {
	Version          [3]int
	CompressionLevel int
	MinFileSize      int
	MaxVersions      int
	Ciphers          []string
}

func (rcfg *RepoConfig) Write(path string) error {
	// Write the properties file
	ff, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, os.ModePerm)
	if err != nil {
		return err
	}
	defer ff.Close()

	enc := toml.NewEncoder(ff)
	err = enc.Encode(rcfg)
	if err != nil {
		return err
	}

	return nil
}

func (rcfg *RepoConfig) Load(path string) error {
	_, err := os.Stat(path)
	if err == nil {
		_, err = toml.DecodeFile(path, rcfg)
	}

	return err
}

func (rcfg *RepoConfig) Init() {
	rcfg.CompressionLevel = 5
	rcfg.MinFileSize = 1024
	rcfg.Version = [3]int{1, 0, 0}
	rcfg.MaxVersions = 4
	rcfg.Ciphers = []string{"aes"}
}
