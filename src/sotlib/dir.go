package sotlib

import (
	"bufio"
	"bytes"
	"compress/zlib"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"golang.org/x/crypto/twofish"

	"github.com/golang/protobuf/proto"
	"github.com/satori/go.uuid"
)

const (
	RootUuid = "7f5e4fcc312f49bf8a057dbd0008c7af"
	RootPath = "7f/5e4fcc312f49bf8a057dbd0008c7af"
	RootDir  = "7f"
	RootFile = "5e4fcc312f49bf8a057dbd0008c7af"

	EntryTypeDir  = DirEntryType_DIR
	EntryTypeFile = DirEntryType_FILE

	DirEyecatcher = "clouddir"
)

type FileInfo interface {
	Name() string
	Size() int64
	Mode() os.FileMode
	ModTime() time.Time
	IsDir() bool

	Uuid() string

	Checksum() []byte
}

type fileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
	isDir   bool

	uuid string

	checksum []byte
}

func (fi *fileInfo) Name() string {
	return fi.name
}

func (fi *fileInfo) Size() int64 {
	return fi.size
}

func (fi *fileInfo) Mode() os.FileMode {
	return fi.mode
}

func (fi *fileInfo) ModTime() time.Time {
	return fi.modTime
}

func (fi *fileInfo) IsDir() bool {
	return fi.isDir
}

func (fi *fileInfo) Uuid() string {
	return fi.uuid
}

func (fi *fileInfo) Checksum() []byte {
	return fi.checksum
}

func (fi *fileInfo) Sys() interface{} {
	return nil
}

func (de *DirEntry) IsFile() bool {
	return de.Type == DirEntryType_FILE
}

func (de *DirEntry) IsDir() bool {
	return de.Type == DirEntryType_DIR
}

func (de *DirEntry) LatestFileInfo(name string) FileInfo {
	vv := de.Versions[de.Latest]
	return &fileInfo{name, vv.Size, os.FileMode(vv.Attributes), time.Unix(vv.ModTime, 0), de.IsDir(), vv.Uuid, vv.Checksum}
}

func (de *DirEntry) FileInfo(name string, version int) FileInfo {
	vid := (int(de.Latest) + version) % len(de.Versions)
	vv := de.Versions[vid]
	return &fileInfo{name, vv.Size, os.FileMode(vv.Attributes), time.Unix(vv.ModTime, 0), de.IsDir(), vv.Uuid, vv.Checksum}
}

func (de *DirEntry) Uuid() string {
	return de.Versions[de.Latest].Uuid
}

func (de *DirEntry) ModTime() int64 {
	return de.Versions[de.Latest].ModTime
}

func (de *DirEntry) Size() int64 {
	return de.Versions[de.Latest].Size
}

func (de *DirEntry) Checksum() []byte {
	return de.Versions[de.Latest].Checksum
}

func (de *DirEntry) Attributes() uint64 {
	return de.Versions[de.Latest].Attributes
}

type DiffStatus struct {
	Newer        int // positive - the entry is newer, 0 - the same, negative the other is newer
	SizeDiff     bool
	ChecksumDiff bool
}

type Dir struct {
	base BaseDir

	ivs [][]byte

	path     string // The sot [cannonical] path to this directory
	filePath string // The path to the file containing the directory contents

	dirty bool
	sett  *Settings
}

func generateUuid() string {
	uu := uuid.NewV4().String()
	return strings.Replace(uu, "-", "", -1)
}

func NewRootDir(settings *Settings) *Dir {
	return &Dir{ivs: settings.GenerateIvs(), path: "/", filePath: path.Join(settings.GetRootPath(), "objects", RootPath), sett: settings, base: BaseDir{make(map[string]*DirEntry), DirEyecatcher}}
}

func makeNewDir(settings *Settings) (*Dir, string, error) {
	// Generate an iv for this directory
	dir := &Dir{ivs: settings.GenerateIvs(), sett: settings, base: BaseDir{make(map[string]*DirEntry), DirEyecatcher}}

	// Generate the uuid
	uu := generateUuid()

	// Create the subdirectory (a noop if it already exists)
	os.Mkdir(path.Join(dir.sett.GetRootPath(), "objects", uu[:2]), os.ModePerm)

	// Setup the path to the file containing the directory contents
	dir.filePath = dir.uuidToPath(uu)

	err := dir.Write()
	if err != nil {
		return nil, "", err
	}

	return dir, uu, nil
}

func OpenDir(settings *Settings, fullPath string, create bool, filePathOk bool) (*Dir, bool, error) {
	dir, err := OpenRoot(settings)
	if err != nil {
		return nil, false, err
	}

	paths := PathComponents(fullPath)

	first := 0
	if paths[first] == "/" {
		first = 1
	}

	last := len(paths) - 1

	for ; first <= last; first++ {
		de, ok := dir.base.Entries[paths[first]]
		if ok {
			if de.IsFile() {
				if first == last && filePathOk {
					return dir, true, nil
				} else {
					return nil, false, errors.New("The destination path [" + fullPath + "] has non-directory components in it.")
				}
			}

			dir.path = path.Join(dir.path, paths[first])
			dir.filePath = dir.uuidToPath(de.Uuid())
			err = dir.Read()
			if err != nil {
				return nil, false, err
			}
		} else {
			if create {
				newDir, uu, err := makeNewDir(settings)
				if err != nil {
					return nil, false, err
				}

				// Add the new directory to the entry list
				ve := make([]*VersionEntry, 1)
				ve[0] = &VersionEntry{Uuid: uu, ModTime: time.Now().Unix()}
				dir.base.Entries[paths[first]] = &DirEntry{Type: EntryTypeDir, Versions: ve, Latest: 0}
				// Write the updated directory
				err = dir.Write()
				if err != nil {
					return nil, false, err
				}

				// Reset the current directory to the one created
				dir.path = path.Join(dir.path, paths[first])
				dir.filePath = newDir.filePath
				dir.ivs = newDir.ivs
				dir.base.Entries = newDir.base.Entries
			} else {
				return nil, false, errors.New("The destination path [" + fullPath + "] doesn't exist.")
			}
		}
	}

	return dir, false, nil
}

func (dir *Dir) uuidToPath(uuid string) string {
	return path.Join(dir.sett.GetRootPath(), "objects", uuid[0:2], uuid[2:])
}

func (dir *Dir) getUuid() string {
	return path.Base(path.Dir(dir.filePath)) + path.Base(dir.filePath)
}

func PathComponents(thepath string) []string {
	res := []string{}
	for true {
		res = append([]string{path.Base(thepath)}, res...)
		pp := path.Dir(thepath)
		if pp == thepath {
			break
		}
		thepath = pp
	}

	return res
}

func (dir *Dir) AddDir(name string, existingPath string) error {
	_, ok := dir.base.Entries[name]
	if !ok {
		_, uu, err := makeNewDir(dir.sett)
		if err != nil {
			return err
		}

		if existingPath == "" {
			// No real directory - use the root directory
			existingPath = path.Join(dir.sett.GetRootPath(), "objects", RootPath)
		}

		fi, err := os.Stat(existingPath)
		if err != nil {
			return err
		}
		permissions := uint64(fi.Mode())

		// Add the new directory to the entry list
		ve := make([]*VersionEntry, 1)
		ve[0] = &VersionEntry{Uuid: uu, ModTime: time.Now().Unix(), Attributes: permissions}
		dir.base.Entries[name] = &DirEntry{Type: EntryTypeDir, Versions: ve, Latest: 0}
		// Write the updated directory
		dir.Write()
	}
	return nil
}

func OpenRoot(settings *Settings) (*Dir, error) {
	dir := &Dir{filePath: path.Join(settings.GetRootPath(), "objects", RootPath), path: "/", dirty: false, sett: settings}
	err := dir.Read()
	if err != nil {
		return nil, err
	}
	return dir, nil
}

func (dir *Dir) readFileHeader(rr io.Reader) (*FileHeader, error) {
	var headerLen int32
	err := binary.Read(rr, binary.LittleEndian, &headerLen)
	if err != nil {
		return nil, err
	}

	buffer := make([]byte, headerLen)
	nn, err := rr.Read(buffer)
	if err != nil {
		return nil, err
	}

	if nn != len(buffer) {
		return nil, errors.New("Reading failed [tried ]" + string(headerLen) + " bytes, read " + string(nn) + " bytes].")
	}

	var res FileHeader
	err = proto.Unmarshal(buffer, &res)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

func (dir *Dir) chainCipherReaders(fh FileHeader, firstReader io.Reader) ([]cipher.StreamReader, error) {
	var res []cipher.StreamReader

	lastReader := firstReader

	for ii := len(fh.Ciphers) - 1; ii >= 0; ii-- {
		var block cipher.Block
		var err error
		switch fh.Ciphers[ii] {
		case "aes":
			block, err = aes.NewCipher(dir.sett.GetKey(ii))
			if err != nil {
				return nil, err
			}
		case "twofish":
			block, err = twofish.NewCipher(dir.sett.GetKey(ii))
			if err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("Unknown cipher '" + fh.Ciphers[ii] + "' specified.")
		}
		stream := cipher.NewCTR(block, fh.Ivs[ii])
		res = append(res, cipher.StreamReader{S: stream, R: lastReader})

		lastReader = res[len(res)-1]
	}
	return res, nil
}

func (dir *Dir) Read() error {

	// The flow is: read file -> decrypt -> uncompress -> load from protobuf

	r1, err := os.Open(dir.filePath)
	if err != nil {
		return err
	}

	fh, err := dir.readFileHeader(r1)
	if err != nil {
		return err
	}

	dir.ivs = fh.Ivs

	var lastReader io.Reader = r1

	streamReaders, err := dir.chainCipherReaders(*fh, lastReader)
	if err != nil {
		return err
	}

	if len(streamReaders) > 0 {
		lastReader = streamReaders[len(streamReaders)-1]
	}

	r3, err := zlib.NewReader(lastReader)

	bb, err := ioutil.ReadAll(r3)
	if err != nil {
		return err
	}

	err = proto.Unmarshal(bb, &dir.base)
	if err != nil {
		return err
	}

	// Protocol buffers leave the Entries map to nil if it's empty
	if nil == dir.base.Entries {
		dir.base.Entries = make(map[string]*DirEntry)
	}

	// fmt.Println("\nRead a directory")
	// dir.PrintEntries()

	dir.dirty = false

	return nil
}

func (dir *Dir) getMinFileSize() int {
	res := dir.sett.GetMinFileSize()
	if res < IV_LEN {
		res = IV_LEN
	} else {
		res = res - IV_LEN
	}
	return res
}

func (dir *Dir) Write() error {

	// fmt.Println("\nWriting a directory")
	// dir.PrintEntries()

	w1, err := os.OpenFile(dir.filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, os.ModePerm)
	if err != nil {
		return err
	}
	defer w1.Close()

	// Serialize
	fh := FileHeader{CompressionType: CompressionType_ZLIB, CompressionLevel: int32(dir.sett.RepoCfg.CompressionLevel), Ciphers: dir.sett.RepoCfg.Ciphers}
	fh.Ivs = dir.ivs

	err = dir.writeFileHeader(w1, fh)
	if err != nil {
		return err
	}

	var lastWriter io.Writer = w1

	streamWriters, err := dir.chainCipherWriters(fh, lastWriter)
	if err != nil {
		return err
	}

	// Defer the closing of all writers
	for _, ww := range streamWriters {
		defer ww.Close()
	}

	if len(streamWriters) > 0 {
		lastWriter = streamWriters[len(streamWriters)-1]
	}

	w2 := bufio.NewWriter(lastWriter)
	buffer, err := proto.Marshal(&dir.base)
	if err != nil {
		return err
	}

	w3 := bufio.NewWriter(w2)

	w4 := NewMinWriter(w3, dir.getMinFileSize())

	w5, err := zlib.NewWriterLevel(w4, dir.sett.GetCompressionLevel())
	if err != nil {
		return err
	}
	defer w5.Close()

	_, err = w5.Write(buffer)
	if err != nil {
		return err
	}

	// Closing the zlib writer first causes it to write out all it's data. Thus,
	// when we close the MinWriter [w4], it will properly enforce the desired size.
	w5.Close()
	w4.Close()
	w3.Flush()

	dir.dirty = false

	return err
}

func (dir *Dir) ChdirDown(subDir string) error {
	de, ok := dir.base.Entries[subDir]
	if ok {
		if de.IsFile() {
			return errors.New("The entry is a file, not a directory.")
		}

		dir.path = path.Join(dir.path, subDir)
		dir.filePath = dir.uuidToPath(de.Uuid())
		err := dir.Read()
		if err != nil {
			return err
		}
	} else {
		return errors.New("The sub directory " + subDir + " doesn't exist.")
	}

	return nil
}

func (dir *Dir) RemoveEntry(name string) error {
	de, ok := dir.base.Entries[name]
	if ok {
		for _, ve := range de.Versions {
			err := os.Remove(dir.uuidToPath(ve.Uuid))
			if err != nil {
				return err
			}
		}
		delete(dir.base.Entries, name)
		dir.dirty = true
	}
	return nil
}

func (dir *Dir) RemoveFileEntry(name string, de *DirEntry) error {
	err := os.Remove(dir.uuidToPath(de.Uuid()))
	if err != nil {
		return err
	}
	delete(dir.base.Entries, name)

	dir.dirty = true

	return nil
}

func (dir *Dir) buildFileHeader() (FileHeader, error) {
	res := FileHeader{CompressionType: CompressionType_ZLIB, CompressionLevel: int32(dir.sett.RepoCfg.CompressionLevel), Ciphers: dir.sett.RepoCfg.Ciphers}
	for _, _ = range res.Ciphers {
		res.Ivs = append(res.Ivs, GenerateIv())
	}
	return res, nil
}

func (dir *Dir) writeFileHeader(ww io.Writer, fh FileHeader) error {
	headerBuffer, err := proto.Marshal(&fh)
	if err != nil {
		return err
	}

	// Write out the header length
	var headerLen int32 = int32(len(headerBuffer))

	err = binary.Write(ww, binary.LittleEndian, headerLen)
	if err != nil {
		return err
	}

	// Write out the serialized header
	nn, err := ww.Write(headerBuffer)
	if err != nil {
		return err
	}

	if nn != len(headerBuffer) {
		return errors.New("Writing failed [tried ]" + string(len(headerBuffer)) + " bytes, wrote " + string(nn) + " bytes].")
	}
	return err
}

func (dir *Dir) chainCipherWriters(fh FileHeader, firstWriter io.Writer) ([]cipher.StreamWriter, error) {
	var res []cipher.StreamWriter

	lastWriter := firstWriter

	for kk, vv := range fh.Ciphers {
		var block cipher.Block
		var err error
		switch vv {
		case "aes":
			block, err = aes.NewCipher(dir.sett.GetKey(kk))
			if err != nil {
				return nil, err
			}
		case "twofish":
			block, err = twofish.NewCipher(dir.sett.GetKey(kk))
			if err != nil {
				return nil, err
			}
		default:
			return nil, errors.New("Unknown cipher '" + vv + "' specified.")
		}
		stream := cipher.NewCTR(block, fh.Ivs[kk])
		res = append(res, cipher.StreamWriter{S: stream, W: lastWriter})

		lastWriter = res[len(res)-1]
	}
	return res, nil
}

func (dir *Dir) encodeToFileTemp(src string) (*os.File, []byte, error) {
	w1, err := ioutil.TempFile("", "")
	if err != nil {
		return nil, nil, err
	}

	fh, err := dir.buildFileHeader()
	if err != nil {
		return w1, nil, err
	}

	err = dir.writeFileHeader(w1, fh)
	if err != nil {
		return w1, nil, err
	}

	var lastWriter io.Writer = w1

	streamWriters, err := dir.chainCipherWriters(fh, lastWriter)
	if err != nil {
		return w1, nil, err
	}

	// Defer the closing of all writers
	for _, ww := range streamWriters {
		defer ww.Close()
	}

	if len(streamWriters) > 0 {
		lastWriter = streamWriters[len(streamWriters)-1]
	}

	// fmt.Printf("%v\n", fh)

	w3 := bufio.NewWriter(lastWriter)

	w4 := NewMinWriter(w3, dir.getMinFileSize())
	defer w4.Close()

	w5, err := zlib.NewWriterLevel(w4, dir.sett.GetCompressionLevel())
	if err != nil {
		return w1, nil, err
	}
	defer w5.Close()

	w6 := sha256.New()

	w7 := io.MultiWriter(w5, w6)

	r1, err := os.Open(src)
	if err != nil {
		return w1, nil, err
	}
	defer r1.Close()

	_, err = io.Copy(w7, r1)
	if err != nil {
		return w1, nil, err
	}

	// Closing the zlib writer first causes it to write out all it's data. Thus,
	// when we close the MinWriter [w4], it will properly enforce the desired size.
	w5.Close()
	w4.Close()
	w3.Flush()

	return w1, w6.Sum(nil), nil
}

func (dir *Dir) AddFile(src string, dest string, force bool) (error, bool) {
	if dest == "" {
		dest = path.Base(src)
	}

	var err error
	var tt *os.File = nil
	var checksum []byte = nil

	de, ok := dir.base.Entries[dest]

	if ok {
		changeDetected := force
		if !changeDetected {
			fi, err := os.Stat(src)
			if err != nil {
				return err, false
			}
			// Compare the file time
			if fi.ModTime().Unix() > de.ModTime() {
				// Compare the size
				if fi.Size() != de.Size() {
					changeDetected = true
				} else {
					// Compute the checksum
					tt, checksum, err = dir.encodeToFileTemp(src)
					if err != nil {
						return err, false
					}
					// Compare the hash
					changeDetected = changeDetected || (bytes.Compare(checksum, de.Checksum()) != 0)
				}

				if !changeDetected {
					// Update the timestamp only
					de.Versions[de.Latest].ModTime = fi.ModTime().Unix()
					return nil, true
				}
			}
		}

		if !changeDetected {
			return nil, false
		}

		// Encode the file if necessary
		if tt == nil {
			tt, checksum, err = dir.encodeToFileTemp(src)
			if err != nil {
				return err, false
			}
		}

		fi, err := os.Stat(src)
		if err != nil {
			return err, false
		}

		// The new version entry
		newVersionEntry := &VersionEntry{Uuid: generateUuid(), ModTime: fi.ModTime().Unix(), Attributes: uint64(fi.Mode()), Size: fi.Size(), Checksum: checksum}

		var veBackup *VersionEntry = nil

		// Are we adding a new version or updating the oldest?
		if len(de.Versions) >= dir.sett.GetMaxVersions() {
			de.Latest = (de.Latest + 1) % int32(dir.sett.GetMaxVersions())
			veBackup = de.Versions[de.Latest]
			de.Versions[de.Latest] = newVersionEntry
		} else {
			de.Versions = append(de.Versions, newVersionEntry)
			de.Latest = de.Latest + 1
		}

		// The in-memory directory is updated, perform the file operations

		actualPath := dir.uuidToPath(newVersionEntry.Uuid)

		// Create the subdirectory (a noop if it already exists)
		os.Mkdir(path.Join(dir.sett.GetRootPath(), "objects", newVersionEntry.Uuid[:2]), os.ModePerm)

		os.Rename(tt.Name(), actualPath)

		// Remove the previous version if necessary
		if veBackup != nil {
			os.Remove(dir.uuidToPath(veBackup.Uuid))
		}

		dir.Write()
	} else {
		// Encode the file
		tt, checksum, err = dir.encodeToFileTemp(src)
		if err != nil {
			return err, false
		}

		fi, err := os.Stat(src)
		if err != nil {
			return err, false
		}

		// Add a new entry
		de = &DirEntry{Type: EntryTypeFile, Latest: 0}
		newVersionEntry := &VersionEntry{Uuid: generateUuid(), ModTime: fi.ModTime().Unix(), Attributes: uint64(fi.Mode()), Size: fi.Size(), Checksum: checksum}
		de.Versions = append(de.Versions, newVersionEntry)

		// The in-memory directory is updated, perform the file operations

		actualPath := dir.uuidToPath(newVersionEntry.Uuid)

		// Create the subdirectory (a noop if it already exists)
		os.Mkdir(path.Join(dir.sett.GetRootPath(), "objects", newVersionEntry.Uuid[:2]), os.ModePerm)

		os.Rename(tt.Name(), actualPath)

		dir.base.Entries[dest] = de
		dir.Write()
	}

	return nil, true
}

func (dir *Dir) GetFile(src string, dest string, force bool) (bool, error) {
	de, ok := dir.base.Entries[src]
	if !ok {
		return false, errors.New("File '" + src + "' doesn't exists.")
	}

	if !force {
		fi, err := os.Stat(dest)
		if err == nil && fi.ModTime().Unix() > de.ModTime() {
			return false, nil
		}
	}

	w1, err := os.OpenFile(dest, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, os.FileMode(de.Attributes()))
	if err != nil {
		return false, nil
	}
	defer w1.Close()

	// The flow is: read file -> decrypt -> uncompress

	r1, err := os.Open(dir.uuidToPath(de.Uuid()))
	if err != nil {
		return false, nil
	}
	defer r1.Close()

	fh, err := dir.readFileHeader(r1)
	if err != nil {
		return false, nil
	}

	var lastReader io.Reader = r1

	streamReaders, err := dir.chainCipherReaders(*fh, lastReader)
	if err != nil {
		return false, nil
	}

	if len(streamReaders) > 0 {
		lastReader = streamReaders[len(streamReaders)-1]
	}

	r3, err := zlib.NewReader(lastReader)
	if err != nil {
		return false, nil
	}
	defer r3.Close()

	_, err = io.Copy(w1, r3)
	if err != nil {
		return false, nil
	}

	tt := time.Unix(de.ModTime(), 0)
	os.Chtimes(dest, tt, tt)

	return true, nil
}

func (dir *Dir) WriteIfNeeded() error {
	if dir.dirty {
		return dir.Write()
	}
	return nil
}

func (dir *Dir) PrintEntries() {
	fmt.Println("    path: " + dir.path)
	fmt.Println("    file path: " + dir.filePath)
	if dir.base.Entries == nil {
		fmt.Println("    entries is nil")
	} else {
		for kk, vv := range dir.base.Entries {
			typeStr := "file"
			if vv.IsDir() {
				typeStr = "dir"
			}
			fmt.Println("    " + kk + ": type = " + typeStr)
			// Print the version
			for vid, ve := range vv.Versions {
				fmt.Println("        " + string(vid) + ": uuid = " + ve.Uuid + "; size = " + strconv.FormatInt(ve.Size, 10))
			}

		}
	}

	fmt.Println("")
}

func (dir *Dir) LookupEntry(name string) (*DirEntry, bool) {
	de, ok := dir.base.Entries[name]
	return de, ok
}

func (dir *Dir) GetEntries() map[string]*DirEntry {
	return dir.base.Entries
}

func (dir *Dir) GetNumEntries() int {
	return len(dir.base.Entries)
}

func (dir *Dir) RemoveAllEntries() error {
	for _, vv := range dir.base.Entries {
		for _, ve := range vv.Versions {
			err := os.Remove(dir.uuidToPath(ve.Uuid))
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func computeChecksum(localPath string) (error, []byte) {
	ww := sha256.New()

	rr, err := os.Open(localPath)
	if err != nil {
		return err, nil
	}
	defer rr.Close()

	_, err = io.Copy(ww, rr)
	if err != nil {
		return err, nil
	}

	return nil, ww.Sum(nil)
}

func (de *DirEntry) Diff(local string) (error, *DiffStatus) {
	res := &DiffStatus{}

	fi, err := os.Stat(local)
	if err != nil {
		return err, nil
	}

	res.SizeDiff = fi.Size() != de.Size()

	// Compare the file time
	if fi.ModTime().Unix() > de.ModTime() {
		res.Newer = -1

	} else if fi.ModTime().Unix() < de.ModTime() {
		res.Newer = 1
	}

	err, cs := computeChecksum(local)
	if err != nil {
		return err, nil
	}
	res.ChecksumDiff = bytes.Compare(cs, de.Checksum()) != 0

	return nil, res
}
