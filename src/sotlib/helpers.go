package sotlib

import (
	"crypto/rand"
	"fmt"
	"os"
	"path"
	"path/filepath"
)

const IV_LEN int = 16

func GenerateIv() []byte {
	res := make([]byte, IV_LEN)
	rand.Read(res)
	return res
}

func GetAesIv(allIvs []byte) []byte {
	return allIvs[:16]
}

func GetTwofishIv(allIvs []byte) []byte {
	return allIvs[16:32]
}

func Println(settings *Settings, msg string) {
	if settings.Verbose {
		fmt.Println(msg)
	}
}

func CanonicalPath(pp string) string {
	return path.Clean(filepath.ToSlash(pp))
}

func ListUuids(settings *Settings) ([]string, error) {
	var res []string

	callBack := func(pp string, fi os.FileInfo, err error) error {
		if !fi.IsDir() {
			dirPath := path.Dir(pp)
			res = append(res, path.Base(dirPath)+path.Base(pp))
		}
		return err
	}

	err := filepath.Walk(path.Join(settings.GetRootPath(), "objects"), callBack)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func ListObjects(settings *Settings, startPath string) ([]FileInfo, error) {
	var res []FileInfo

	callBack := func(pp string, fis []FileInfo, err error) error {
		// fmt.Printf("path: %s, uuid: %s\n", pp, fi.Uuid())
		res = append(res, fis...)
		return err
	}

	err := Walk(settings, startPath, callBack)
	if err != nil {
		return nil, err
	}

	return res, nil
}
