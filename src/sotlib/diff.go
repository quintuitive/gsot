package sotlib

import (
	"errors"
	"os"
	"path"
	"path/filepath"

	"github.com/oleiade/lane"
)

type DiffResult struct {
	localPath string // The path to the local entry
	repoPath  string // The path to the repository entry
	localType int    // 0 - doesn't exist, 1 - file, 2 - dir
	repoType  int    // 0 - doesn't exist, 1 - file, 2 - dir
}

func DiffEntries(settings *Settings, src string, dest string) ([]DiffResult, error) {
	// Accept only UNIX style paths as destination
	if dest != filepath.ToSlash(dest) {
		return nil, errors.New("The destionation path [" + dest + "] doesn't seem to be a valid UNIX-style path.")
	}

	src = path.Clean(filepath.ToSlash(src))

	fi, err := os.Stat(src)
	if err != nil {
		return nil, err
	}

	queue := lane.NewQueue()

	srcIsDir := fi.IsDir()

	var res []DiffResult

	if srcIsDir {
		queue.Append(DirPair{src, dest})
	} else {
		pp := path.Clean(dest)
		destDir := path.Dir(pp)
		dir, _, err := OpenDir(settings, destDir, false, false)
		if err != nil {
			return nil, err
		}

		// Lookup the last path element into the directory
		destFile := path.Base(pp)
		_, ok := dir.LookupEntry(destFile)
		if ok {
			res = append(res, DiffResult{src, dest, 1, 1})
		} else {
			res = append(res, DiffResult{src, dest, 1, 0})
		}
	}

	// false means "don't recurse". Used either for files,
	// or when one side is a file, the other a directory.
	localUuids := make(map[string]bool)

	// Process the queue
	for !queue.Empty() {
		// Pop the head of the queue
		dp := queue.Dequeue()

		destPath := dp.(DirPair).dest
		srcPath := dp.(DirPair).src

		dir, _, err := OpenDir(settings, path.Dir(destPath), false, false)
		if err != nil {
			return nil, err
		}

		baseDestPath := path.Base(destPath)
		de, ok := dir.LookupEntry(baseDestPath)
		if !ok {
			res = append(res, DiffResult{src, dest, 2, 0})
			continue
		}

		if de.IsFile() {
			localUuids[de.Uuid()] = false
			res = append(res, DiffResult{src, dest, 2, 1})
			continue
		} else {
			localUuids[de.Uuid()] = true
			res = append(res, DiffResult{src, dest, 2, 2})
		}

		// Enter the repository sub directory
		dir.ChdirDown(baseDestPath)

		// Process all directory entries. Files are added to the repo directly,
		// sub directories are queued for further processing.
		realDir, err := os.Open(srcPath)
		if err != nil {
			return nil, err
		}

		rdes, err := realDir.Readdir(-1)
		if err != nil {
			return nil, err
		}

		for _, rde := range rdes {
			// fmt.Println("Processing: " + rde.Name())
			if rde.IsDir() {
				fullSrcPath := path.Join(dp.(DirPair).src, rde.Name())
				queue.Append(DirPair{fullSrcPath, path.Join(destPath, rde.Name())})
			} else {
				// A file - diff it

				fullSrcPath := path.Join(dp.(DirPair).src, rde.Name())
				fullDestPath := path.Join(dp.(DirPair).dest, rde.Name())

				de, ok := dir.LookupEntry(rde.Name())
				if !ok {
					res = append(res, DiffResult{fullSrcPath, fullDestPath, 1, 0})
					continue
				}

				if de.IsDir() {
					res = append(res, DiffResult{fullSrcPath, fullDestPath, 1, 2})
					localUuids[de.Uuid()] = true
					continue
				} else {
					localUuids[de.Uuid()] = false
					res = append(res, DiffResult{fullSrcPath, fullDestPath, 1, 1})
				}
			}
		}
	}

	if srcIsDir {
		// Scan the object tree
		callBack := func(ss string, fis []FileInfo, err error) error {
			// fmt.Printf("path: %s, uuid: %s\n", pp, fi.Uuid())
			bb, ok := localUuids[fis[0].Uuid()]
			if !ok {
				if fis[0].IsDir() {
					res = append(res, DiffResult{"", ss, 0, 2})
				} else {
					res = append(res, DiffResult{"", ss, 0, 1})
				}
				if fis[0].IsDir() {
					return SkipDir
				}
			} else if !bb && fis[0].IsDir() {
				return SkipDir
			}

			return nil
		}

		err := Walk(settings, dest, callBack)
		if err != nil {
			return nil, err
		}
	}

	return res, nil
}
