package sotlib

import (
	"crypto/sha512"
	"encoding/base64"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/user"
	"path"
	"strings"

	"github.com/BurntSushi/toml"
	"golang.org/x/crypto/pbkdf2"
)

const (
	// Up to three ciphers are allowed. Each takes a 256 bit key.
	KEY_LEN = 3 * 32
)

type SavedSettings struct {
	RootPath string
}

type Settings struct {
	Key []byte

	CreateParents bool // mkdir
	Force         bool
	LongFormat    bool // ls
	Verbose       bool
	DryRun        bool
	Versions      bool // ls
	All           bool // push,pull
	Bits          int  // keygen
	Hex           bool // keygen

	RepoCfg RepoConfig

	Saved SavedSettings

	Cred Credentials
}

func InitAndLoadSettings() Settings {
	res := Settings{}
	res.Init()
	res.LoadSaved()
	res.LoadCredentials()
	return res
}

func (ss *Settings) CreateKey() error {
	if len(ss.Cred.KeyFile) > 0 {
		var pp string
		if path.IsAbs(ss.Cred.KeyFile) {
			pp = ss.Cred.KeyFile
		} else {
			usr, err := user.Current()
			if err != nil {
				return err
			}

			pp = path.Join(usr.HomeDir, ".sot", ss.Cred.KeyFile)
		}

		ff, err := os.OpenFile(pp, os.O_RDONLY, os.ModePerm)
		if err != nil {
			return err
		}
		defer ff.Close()

		bb, err := ioutil.ReadAll(ff)
		if err != nil {
			return err
		}

		ss.Key, err = base64.StdEncoding.DecodeString(strings.TrimSpace(string(bb)))
		if err != nil {
			return err
		}

		if len(ss.Key) < KEY_LEN {
			return errors.New(fmt.Sprintf("The key is too short [%d bytes], must be at least %d bytes long", len(ss.Key), KEY_LEN))
		}
	} else if len(ss.Cred.Password) > 0 {
		ss.Key = pbkdf2.Key([]byte(ss.Cred.Password), []byte(""), 8192, KEY_LEN, sha512.New)
	} else {
		return errors.New("Either password or a key [base64 encoded] or a key file [binary] must be specified")
	}
	return nil
}

func (ss *Settings) GetCompressionLevel() int {
	return ss.RepoCfg.CompressionLevel
}

func (ss *Settings) GetMinFileSize() int {
	return ss.RepoCfg.MinFileSize
}

func (ss *Settings) GetMaxVersions() int {
	return ss.RepoCfg.MaxVersions
}

func (ss *Settings) GetKey(id int) []byte {
	return ss.Key[(id * 32):((id + 1) * 32)]
}

func (ss *Settings) GetRootPath() string {
	return ss.Saved.RootPath
}

func (ss *Settings) GetRepoCfg() *RepoConfig {
	return &ss.RepoCfg
}

func (ss *Settings) LoadCredentials() {
	ss.Cred.Load()
}

func (ss *Settings) LoadSaved() {
	ss.Saved.Load()
}

func (ss *Settings) WriteSaved() {
	ss.Saved.Write()
}

func (ss *Settings) GetPassword() string {
	return ss.Cred.Password
}

func (ss *Settings) GetKeyFile() string {
	return ss.Cred.KeyFile
}

func (ss *SavedSettings) Load() {
	usr, err := user.Current()
	if err != nil {
		return
	}

	pp := path.Join(usr.HomeDir, ".sot", "saved-settings.toml")

	if _, err := os.Stat(pp); err == nil {
		toml.DecodeFile(pp, &ss)
	}

	ss.RootPath = CanonicalPath(ss.RootPath)
}

func (ss *SavedSettings) Write() {

	ss.RootPath = CanonicalPath(ss.RootPath)

	usr, err := user.Current()
	if err != nil {
		return
	}

	pp := path.Join(usr.HomeDir, ".sot")
	err = os.Mkdir(pp, os.ModePerm)

	pp = path.Join(pp, "saved-settings.toml")

	ff, err := os.OpenFile(pp, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, os.ModePerm)
	if err != nil {
		return
	}
	defer ff.Close()

	enc := toml.NewEncoder(ff)
	err = enc.Encode(ss)
	if err != nil {
		return
	}
}

func (ss *SavedSettings) Init() {
}

func (sett *Settings) LoadRepoConfig() error {
	pp := path.Join(sett.Saved.RootPath, "properties.toml")
	return sett.RepoCfg.Load(pp)
}

func (sett *Settings) WriteRepoConfig() error {
	pp := path.Join(sett.Saved.RootPath, "properties.toml")
	return sett.RepoCfg.Write(pp)
}

func (sett *Settings) Init() {
	sett.RepoCfg.Init()
	sett.Saved.Init()
	sett.Cred.Init()
}

func (sett *Settings) GenerateIvs() [][]byte {
	var res [][]byte
	for _, _ = range sett.RepoCfg.Ciphers {
		res = append(res, GenerateIv())
	}
	return res
}
