package main

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"sotlib"
	"strings"
	"testing"
    "fmt"
)

func TestEndToEnd(t *testing.T) {
	tdir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Errorf("Failed to create a temporary directory")
	}
	defer os.RemoveAll(tdir)

	sotExec := "../../bin/sot"

	repoPath := path.Join(tdir, "sotrepo")
	err = os.Mkdir(repoPath, os.ModePerm)

	// fmt.Println("Temp Dir: " + tdir)
	// curDir, err := os.Getwd()
	// fmt.Println("Current Dir: " + curDir)

	password := "secret phrase"
	cmd := exec.Command(sotExec, "init", "--password", password, repoPath)
	err = cmd.Run()
	if err != nil {
		t.Error(err)
	}

	testDir := os.Getenv("SOT_TO_TEST_DIR")
	if len(testDir) == 0 {
		testDir = path.Join("..", "testdata", "books")
	}

	cmd = exec.Command(sotExec, "push", "--repo", repoPath, "--password", password, testDir, "/")
	err = cmd.Run()
	if err != nil {
		t.Error(err)
	}

	cmd = exec.Command(sotExec, "pull", "--repo", repoPath, "--password", password, path.Join("/", path.Base(testDir)), tdir)
	err = cmd.Run()
	if err != nil {
		t.Error(err)
	}

	cmd = exec.Command("diff", "-q", "-r", testDir, path.Join(tdir, path.Base(testDir)))
	out, err := cmd.CombinedOutput()
	// fmt.Println("Output: " + string(out))
	if err != nil {
		t.Error(err)
	}

	if len(out) != 0 {
		t.Error("Non empty output: " + string(out))
	}

	cmd = exec.Command(sotExec, "verify", "--repo", repoPath, "--password", password)
	out, err = cmd.CombinedOutput()
	// fmt.Println("Output: " + string(out))
	if err != nil {
		t.Error(err)
	}

	if strings.TrimSpace(string(out)) != "Verification was successful. No errors were found." {
		t.Error("Unexpected output: '" + string(out) + "'")
	}
}

func TestEndToEnd2(t *testing.T) {
	tdir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Errorf("Failed to create a temporary directory")
	}
	defer os.RemoveAll(tdir)

	sotExec := "../../bin/sot"

	repoPath := path.Join(tdir, "sotrepo")
	err = os.Mkdir(repoPath, os.ModePerm)

	// fmt.Println("Temp Dir: " + tdir)
	// curDir, err := os.Getwd()
	// fmt.Println("Current Dir: " + curDir)

	password := "secret phrase"
	cmd := exec.Command(sotExec, "init", "--password", password, repoPath)
	err = cmd.Run()
	if err != nil {
		t.Error(err)
	}

	testDir := os.Getenv("SOT_TO_TEST_DIR")
	if len(testDir) == 0 {
		testDir = path.Join("..", "testdata", "books")
	}

    testDir += "/"

	cmd = exec.Command(sotExec, "mkdir", "--repo", repoPath, "--password", password, "-p", "dir1/dir2")
	err = cmd.Run()
	if err != nil {
		t.Error(err)
	}

	cmd = exec.Command(sotExec, "push", "--repo", repoPath, "--password", password, testDir, "/dir1/dir2")
	err = cmd.Run()
	if err != nil {
		t.Error(err)
	}

    outDir := path.Join(tdir, "pulled")
    err = os.Mkdir(outDir, os.ModePerm)

	cmd = exec.Command(sotExec, "pull", "--repo", repoPath, "--password", password, "/dir1/dir2/", outDir)
	err = cmd.Run()
	if err != nil {
		t.Error(err)
	}

    t.Log(testDir + ", " + tdir)

	cmd = exec.Command("diff", "-q", "-r", testDir, outDir)
	out, err := cmd.CombinedOutput()
	// fmt.Println("Output: " + string(out))
	if err != nil {
		t.Error(err)
	}

	if len(out) != 0 {
		t.Error("Non empty output: " + string(out))
	}

	cmd = exec.Command(sotExec, "verify", "--repo", repoPath, "--password", password)
	out, err = cmd.CombinedOutput()
	// fmt.Println("Output: " + string(out))
	if err != nil {
		t.Error(err)
	}

	if strings.TrimSpace(string(out)) != "Verification was successful. No errors were found." {
		t.Error("Unexpected output: '" + string(out) + "'")
	}
}

func TestInit(t *testing.T) {

	tdir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Errorf("Failed to create a temporary directory")
	}
	defer os.RemoveAll(tdir)

    fmt.Println("Temp Dir: " + tdir)
    // curDir, err := os.Getwd()
    // fmt.Println("Current Dir: " + curDir)

	var sett sotlib.Settings
	sett.Saved.RootPath = path.Join(tdir, "sotrepo")
	err = os.Mkdir(sett.Saved.RootPath, os.ModePerm)

	sett.Cred.Password = "secret phrase"
	sett.RepoCfg.MinFileSize = 1011
	sett.RepoCfg.Ciphers = []string{"aes"}
	sett.CreateKey()

	err = sotlib.Init(&sett)
	if err != nil {
		t.Error(err)
	}

	rootDirFile := path.Join(sett.GetRootPath(), "objects", "7f", "5e4fcc312f49bf8a057dbd0008c7af")
	fi, err := os.Stat(rootDirFile)
	if err != nil {
		t.Error(err)
	}

	if fi.Size() < int64(sett.RepoCfg.MinFileSize) {
		t.Errorf("The root file has the wrong size")
	}

	// Test chained algos
	os.RemoveAll(sett.Saved.RootPath)
	err = os.Mkdir(sett.Saved.RootPath, os.ModePerm)
	sett.RepoCfg.Ciphers = []string{"aes", "twofish"}

	err = sotlib.Init(&sett)
	if err != nil {
		t.Error(err)
	}

	rootDirFile = path.Join(sett.GetRootPath(), "objects", "7f", "5e4fcc312f49bf8a057dbd0008c7af")
	fi, err = os.Stat(rootDirFile)
	if err != nil {
		t.Error(err)
	}

	// Test no encryption algos
	os.RemoveAll(sett.Saved.RootPath)
	err = os.Mkdir(sett.Saved.RootPath, os.ModePerm)
	sett.RepoCfg.Ciphers = []string{}
	sett.RepoCfg.CompressionLevel = 0

	err = sotlib.Init(&sett)
	if err != nil {
		t.Error(err)
	}

	rootDirFile = path.Join(sett.GetRootPath(), "objects", "7f", "5e4fcc312f49bf8a057dbd0008c7af")
	fi, err = os.Stat(rootDirFile)
	if err != nil {
		t.Error(err)
	}
}
