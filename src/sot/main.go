package main

import (
	"commands"
	"fmt"
)

func main() {

	err := commands.RootCmd.Execute()
	if err != nil {
		fmt.Println(err)
	}

	return

	//	settings := sotlib.NewSettings()
	//
	//	initFlagSet := flag.NewFlagSet("init", flag.ContinueOnError)
	//	initFlagSet.IntVar(&settings.Compression, "compression", 5, "the compression level")
	//	initFlagSet.IntVar(&settings.MinFileSize, "min-file-size", 1024, "minimum file size after compression")
	//	initFlagSet.StringVar(&settings.Password, "password", "", "user password")
	//	initFlagSet.StringVar(&settings.Password, "p", "", "user password")
	//	initFlagSet.StringVar(&settings.Salt, "salt", "", "salt for the user password")
	//
	//	lsFlagSet := flag.NewFlagSet("ls", flag.ContinueOnError)
	//	lsFlagSet.StringVar(&settings.Password, "password", "", "user password")
	//	var longFormat bool
	//	lsFlagSet.BoolVar(&longFormat, "long", false, "show details")
	//	lsFlagSet.BoolVar(&longFormat, "l", false, "show details [shorthand]")
	//
	//	switch os.Args[1] {
	//	case "init":
	//		initFlagSet.Parse(os.Args[2:])
	//	case "ls":
	//		lsFlagSet.Parse(os.Args[2:])
	//	}
	//
	//	if initFlagSet.Parsed() {
	//		err := settings.LoadPassword()
	//		if err != nil {
	//			fmt.Print(err)
	//			return
	//		}
	//
	//		settings.CreateKey()
	//		settings.CreateIv()
	//
	//		args := initFlagSet.Args()
	//
	//		if len(args) < 1 {
	//			settings.RootPath, err = os.Getwd()
	//		} else {
	//			settings.RootPath = args[len(args)-1]
	//		}
	//
	//		var isEmpty bool
	//		isEmpty, err = IsEmpty(settings.RootPath)
	//
	//		if err != nil {
	//			fmt.Fprint(os.Stderr, "Failed to read the directory %s\n", settings.RootPath)
	//			return
	//		}
	//
	//		if !isEmpty {
	//			fmt.Fprintf(os.Stderr, "A new repository is created only in an empty directory and %s is not empty\n", settings.RootPath)
	//			return
	//		}
	//
	//		sotlib.InitTree(settings)
	//	} else if lsFlagSet.Parsed() {
	//		err := settings.LoadPassword()
	//		if err != nil {
	//			fmt.Print(err)
	//			return
	//		}
	//
	//	}
	//

	//	inPath := "/home/ivpop/ttt/test.txt"
	//	outPath := "/home/ivpop/ttt/test.aes"
	//
	//	// inPath := "/home/ivpop/ttt/cmake-3.2.2.tar.gz"
	//	// outPath := "/home/ivpop/ttt/cmake.aes"
	//
	//	key := []byte("example key 1234")
	//
	//	inFile, err := os.Open(inPath)
	//   	if err != nil {
	//   		panic(err)
	//   	}
	//   	defer inFile.Close()
	//
	//   	outFile, err := os.OpenFile(outPath, os.O_WRONLY | os.O_CREATE | os.O_TRUNC, 0644)
	//   	if err != nil {
	//   		panic(err)
	//   	}
	//   	defer outFile.Close()
	//
	//   	mfr := sotlib.NewMinReader(inFile, 1024)
	//
	//   	block, err := aes.NewCipher(key)
	//   	if err != nil {
	//   		panic(err)
	//   	}
	//
	//   	var iv [aes.BlockSize]byte
	//   	stream := cipher.NewOFB(block, iv[:])
	//
	//   	reader := &cipher.StreamReader{S: stream, R: mfr}
	//
	//   	// Copy the input file to the output file, decrypting as we go.
	//   	if _, err := io.Copy(outFile, reader); err != nil {
	//   		panic(err)
	//   	}
	//
	//   	fmt.Println("Success")
}
